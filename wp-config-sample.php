<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'proprofile');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ILjRriQ8 ,,5,MOIyILc]x,dn_eb%LC|(7fP;2E[-tS1!%x_+F87ADug#h`TSH+F');
define('SECURE_AUTH_KEY',  'OQ*mi2y|H)!K/ ^|</;T&;} 1Vo*-0#>^rl$p2^J=w7*&4Gq@s+caqt-a!q^xGb`');
define('LOGGED_IN_KEY',    'hNq3E+KJcu)pyS.DK|?xljFW>I&XTY#LxKqb2+~}<@h-;*Y+8`yZ&$UXVrej%NBk');
define('NONCE_KEY',        'iOL!a{}{ae#~&y2pUY52zd^cz<m2[k!]4Ud/D4$Lo+KC%yL+qRj-U|.NhL9w-p+o');
define('AUTH_SALT',        'pfwa7(zh<eacOsd|seyd#|7-=ZZna++|eG`|b,wk[!Wgt5-[-ZMo{[Ta_2,z]Rr:');
define('SECURE_AUTH_SALT', '%eg3L>8ICNrBh-p*Ur8W+_nE~NT5_I#R%N?;N<5y[v/&GFLqWNyE 3f(46R!Gf#y');
define('LOGGED_IN_SALT',   '>iBvI};`h$s +|/(`3_jMsk-wMlO{w1)ZO:&eD)h{`fIy.BkT{|-n+dy9[0Is!mM');
define('NONCE_SALT',       '2u0&,lvf@ME.F||b!,!yw_<jp_.-1#^GC+.#n3n3z~[=f,g9XL)-nKK?cPU/=Vq.');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

