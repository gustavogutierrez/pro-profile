# Profesional Profile #

Web site para mostrar el perfil profesional de cualquier persona

Home
Blog

 - Implementar Google Adsense (publicidad)
 - Lista de posts
 - Detalle de
   post

Perfil Profesional

 - Breve biografía
 - Habilidades y conocimientos
 - Adjuntar curriculum vita público

Portafolio

- Lista de Categorías
- Proyectos
- Detalle de proyecto
	- Comentario de proyecto
	- Proyectos relacionados
	- Ir a cotizar un proyecto parecido al visualizado
	- Implementar Google Adsense (publicidad)

Cotización de proyecto

- Formulario de cotización
- Registro de usuario cotizante
- Aprobación de proyecto
- Pago de cuotas (Implementando Payu Latam)
- Descarga de factura digital o Cuenta de cobro

Contacto