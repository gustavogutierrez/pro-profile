<?php
namespace App\Plugins;

use Insight\Base\Plugin as BasePlugin;

/**
 * Esta clase sera utilizada para extender a todos los plugin
 * e implementar funcionalidades y parametros comunes a toda la aplicación
 * o ecosistema desarrollado
 */
class Plugin extends BasePlugin {

}
